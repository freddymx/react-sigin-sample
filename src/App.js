/* global gapi */

import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isSignedIn: false,
    }
  }

  componentDidMount() {
    const successCallback = this.onSuccess.bind(this);
    
    // Loads the auth component. window.gapi is available globally as it is 
    // imported on index.html
    window.gapi.load('auth2', () => {
      this.auth2 = gapi.auth2.init({
        client_id: process.env.REACT_APP_GOOGLE_CLIENT_ID,
      });

      this.auth2.then(() => {
        this.onSuccess();
      });
    });

    // Renders the default sign in with google button
    window.gapi.load('signin2', function() {
      const opts = {
        width: 200,
        height: 50,
        client_id: process.env.REACT_APP_GOOGLE_CLIENT_ID,
        onsuccess: successCallback
      }

      gapi.signin2.render('loginButton', opts)
    })
  }

  onSuccess() {
    const profile = this.auth2.currentUser.get().getBasicProfile();

    this.setState({
      isSignedIn: true,
      user: {
        name: profile.getGivenName(),
        avatar: profile.getImageUrl()
      },
      err: null
    });
  }

  onLoginFailed(err) {
    this.setState({
      isSignedIn: false,
      error: err,
    })
  }

  // Left here for simplicity but this would be diff components.
  getContent() {
    if (this.state.isSignedIn === true) {
      return (
        <div>
          <img src={this.state.user.avatar} />
          <p>hello { this.state.user.name }, you're signed in </p>
        </div>
      )
    } else {
      return (
        <div>
          <p>You are not signed in. Click here to sign in.</p>
          <button id="loginButton">Login with Google</button>
        </div>
      )
    }
  }
  
  render() {
    return (      
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Sample App.</h2>
          { this.getContent() }
        </header>
      </div>
    );
  }
}

export default App;
